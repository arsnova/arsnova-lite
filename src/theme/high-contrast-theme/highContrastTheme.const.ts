export const highcontrast = {

  '--primary' : '#fb9a1c',
  '--primary-variant': '#1e1e1e',

  '--secondary': '#fb9a1c',
  '--secondary-variant': '#fb9a1c',

  '--background': '#141414',
  '--surface': '#1e1e1e',
  '--dialog': '#37474f',

  '--on-primary': '#141414',
  '--on-secondary': '#141414',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black'

};

export const highcontrast_meta = {

  'translation': {
    'name': {
      'en': 'High Contrast',
      'de': 'Hoher Kontrast'
    },
    'description': {
      'en': 'Contrast compliant with WCAG 2.1 AA',
      'de': 'Helligkeitskontrast nach WCAG 2.1 AA'
    }
  },
  'order': 0,
  'scale': 1,
  'previewColor': 'secondary'

};


















