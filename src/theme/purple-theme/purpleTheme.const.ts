export const purple = {

  '--primary' : 'Maroon',
  '--primary-variant': 'white',

  '--secondary': 'black',
  '--secondary-variant': 'green',

  '--background': 'beige',
  '--surface': 'white',
  '--dialog': 'white',

  '--on-primary': 'white',
  '--on-secondary': 'white',
  '--on-background': 'black',
  '--on-surface': 'black',
  '--on-cancel': 'white',

  '--green': 'green',
  '--red': 'red',
  '--yellow': 'gold',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black'
};

export const purple_meta = {

  'translation': {
    'name': {
      'en': 'Projector',
      'de': 'Beamer'
    },
    'description': {
      'en': 'Optimized for presentation in lecture halls',
      'de': 'Für die Präsentation im Hörsaal optimiert'
    }
  },
  'order': 1,
  'scale': 1.3,
  'previewColor': 'background'

};
