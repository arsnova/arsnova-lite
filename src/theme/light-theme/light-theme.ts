export const arsnova = {

  '--primary' : '#263238',
  '--primary-variant': '#c0d6e4',

  '--secondary': '#FF6D00',
  '--secondary-variant': '#619790',

  '--background': '#e3eaa7',
  '--surface': '#81C784',
  '--dialog': '#e3eaa7',
  '--cancel': '#BDBDBD',

  '--on-primary': '#ffffff',
  '--on-secondary': '#000000',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--green': 'green',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': '#002878',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': 'black'
};

export const arsnova_meta = {

  'translation': {
    'name': {
      'en': 'Light Mode',
      'de': 'Light Mode'
    },
    'description': {
      'en': 'Bright background for focussed working',
      'de': 'Heller Hintergrund für fokussiertes Arbeiten'
    }
  },
  'order': 2,
  'scale': 1,
  'previewColor': 'background'

};
